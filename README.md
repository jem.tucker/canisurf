# canisurf 
[![Build Status](https://gitlab.com/jem.tucker/canisurf/badges/master/build.svg)](https://gitlab.com/jem.tucker/canisurf/commits/master)
[![Coverage Report](https://gitlab.com/jem.tucker/canisurf/badges/master/coverage.svg)](https://gitlab.com/jem.tucker/canisurf/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/jem.tucker/canisurf)](https://goreportcard.com/report/gitlab.com/jem.tucker/canisurf)

Query your local surf report right from the command line! :surfer:

## Usage

```
[user@host]$ canisurf pipeline
Yes!
```
