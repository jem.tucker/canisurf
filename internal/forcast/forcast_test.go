package forcast

import (
	"testing"
)

func TestForcastIsSurfable(t *testing.T) {
	f := Forcast{0, 0, 0}
	if f.IsSurfable() {
		t.Errorf("IsSurfable should be false")
	}
}
