package forcast

// Forcast contains the surf forcast for a single MSW surf spot at a point in
// time
type Forcast struct {
	Timestamp   int `json:"timestamp"`
	FadedRating int `json:"fadedRating"`
	SolidRating int `json:"solidRating"`
}

// IsSurfable returns true if the spot is surfable
func (f *Forcast) IsSurfable() bool {
	return f.SolidRating > 1 || f.FadedRating > 4
}
