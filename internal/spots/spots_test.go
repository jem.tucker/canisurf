package spots

import "testing"

var spottests = []struct {
	in  string
	out int
}{
	{"brighton", 67},
	{"doesnt-exist", 0},
}

func TestGetSpotID(t *testing.T) {
	for _, test := range spottests {
		num, _ := GetSpotID(test.in)
		if num != test.out {
			t.Errorf("got %d, expected %d", num, test.out)
		}
	}
}
