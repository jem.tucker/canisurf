package spots

import "fmt"

var spotLookup = map[string]int{
	"brighton": 67,
	"pipeline": 616,
}

// GetSpotID looks up a spot by name returning its ID
func GetSpotID(spot string) (int, error) {
	id, found := spotLookup[spot]
	if !found {
		return 0, fmt.Errorf("spot not supported: %s", spot)
	}

	return id, nil
}
